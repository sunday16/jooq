package com.mycompany.myapp.todo;

import com.mycompany.myapp.repository.UserJooqRepository;
import net.mycompany.myapp.jooq.todo.tables.pojos.JhiUser;
import net.mycompany.myapp.jooq.todo.tables.pojos.JhiUserAuthority;
import net.mycompany.myapp.jooq.todo.tables.records.JhiUserRecord;
import org.jooq.DSLContext;
import org.jooq.Record2;
import org.jooq.Result;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.NameTokenizers;
import org.modelmapper.jooq.RecordValueReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.mapping;
import static java.util.stream.Collectors.toList;
import static net.mycompany.myapp.jooq.todo.tables.JhiUser.JHI_USER;
import static net.mycompany.myapp.jooq.todo.tables.JhiUserAuthority.JHI_USER_AUTHORITY;


/**
 * Created by sunday on 02.03.16.
 */
@Repository
public class JooqUser implements UserJooqRepository {

    private final Logger log = LoggerFactory.getLogger(JooqUser.class);
    private final DSLContext jooq;


    @Autowired
    public JooqUser(DSLContext jooq) {
        this.jooq = jooq;

    }

    private UserDto convertQueryResult(JhiUserRecord queryResult) {
        return new UserDto(queryResult.getId(),
            queryResult.getLogin(),
            queryResult.getPasswordHash(),
            queryResult.getFirstName(),
            queryResult.getLastName(),
            queryResult.getEmail(),
            queryResult.getActivated(),
            queryResult.getLangKey(),
            queryResult.getActivationKey(),
            queryResult.getResetKey(),
            queryResult.getResetDate()
        );

    }

    @Transactional
    @Override
    public Result<Record2<Long, String>> findById(Long id) {

        Result<Record2<Long, String>> queryResult = jooq.select(JHI_USER.ID, JHI_USER_AUTHORITY.AUTHORITY_NAME)
            .from(JHI_USER)
            .join(JHI_USER_AUTHORITY)
            .on(JHI_USER.ID.eq(JHI_USER_AUTHORITY.USER_ID))
            .where(JHI_USER.ID.equal(id))

            .orderBy(JHI_USER.ID)
            .fetch();

        queryResult.forEach((r) -> {
            System.out.println(String.format("%s (id: %s)", r.getValue(JHI_USER_AUTHORITY.AUTHORITY_NAME), r.getValue(JHI_USER.ID)));
        });

        if (queryResult == null) {
            log.info(">>>>>>>>>>>>>>>>>>>>>");
            log.info("Jooq queryREsult is null");
            throw new JooqUserNotFound("Not found user for id: " + id);
        }
/*
        UserDto userDto = new UserDto(queryResult.getValue(JHI_USER.ID, JHI_USER.LOGIN, JHI_USER.PASSWORD_HASH, JHI_USER.FIRST_NAME, JHI_USER.LAST_NAME,
            JHI_USER.EMAIL, JHI_USER.ACTIVATED, JHI_USER.LANG_KEY, JHI_USER.ACTIVATION_KEY, JHI_USER.RESET_KEY,
         //   JHI_USER.RESET_DATE, queryResult.getValue(JHI_USER_AUTHORITY.AUTHORITY_NAME));*/

       // log.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
        log.info("queryResult: " + queryResult);
        return queryResult;
    }


    @Override
    public List<UserDto> findAll() {

        List<UserDto> userEntries = new ArrayList<>();

        List<JhiUserRecord> queryResults = jooq.selectFrom(JHI_USER).fetchInto(JhiUserRecord.class);

        for (JhiUserRecord queryResult : queryResults) {
            UserDto userEntry = convertQueryResult(queryResult);
            userEntries.add(userEntry);
        }

        return userEntries;
    }

    @Override
    public Map<JhiUser, List<JhiUserAuthority>> findByIdUser(Long id) {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().addValueReader(new RecordValueReader());
        modelMapper.getConfiguration().setSourceNameTokenizer(NameTokenizers.UNDERSCORE);


        /*JhiUserRecord userRecord = jooq.selectFrom(JHI_USER)
            .where(JHI_USER.ID.equal(id))
            .fetchOne();*/

        Map<JhiUser, List<JhiUserAuthority>> queryResult = jooq.select(JHI_USER.ID, JHI_USER.LOGIN, JHI_USER.PASSWORD_HASH,
            JHI_USER.FIRST_NAME, JHI_USER.LAST_NAME, JHI_USER.EMAIL, JHI_USER.ACTIVATED,
            JHI_USER.LANG_KEY, JHI_USER.ACTIVATION_KEY, JHI_USER.RESET_KEY, JHI_USER.CREATED_BY,
            JHI_USER.CREATED_DATE, JHI_USER.RESET_DATE, JHI_USER.LAST_MODIFIED_BY, JHI_USER.LAST_MODIFIED_DATE,
            JHI_USER_AUTHORITY.AUTHORITY_NAME)
            .from(JHI_USER)
            .join(JHI_USER_AUTHORITY)
            .on(JHI_USER.ID.eq(JHI_USER_AUTHORITY.USER_ID))
            .where(JHI_USER.ID.eq(id))
            .orderBy(JHI_USER.ID)
            .fetch()
            .stream()
            .collect(groupingBy(r -> r.into(JhiUser.class), LinkedHashMap::new,

                mapping(r-> r.into(JhiUserAuthority.class),toList()
                )
            ))
            ;


        /*Map<JhiUser, List<JhiUserAuthority>> q1 = jooq.select()
            .from(JHI_USER)
            .join(JHI_USER_AUTHORITY)
            .on(JHI_USER.ID.eq(JHI_USER_AUTHORITY.USER_ID))
            .fetch()
            .intoGroups(jooq.selectFrom(JhiUserRecord.class), jooq.selectFrom(JHI_USER_AUTHORITY).fetch());*/



       /* Record query = (Record) jooq.select(JHI_USER.ID, JHI_USER.LOGIN, JHI_USER.PASSWORD_HASH,
            JHI_USER.FIRST_NAME, JHI_USER.LAST_NAME, JHI_USER.EMAIL, JHI_USER.ACTIVATED,
            JHI_USER.LANG_KEY, JHI_USER.ACTIVATION_KEY, JHI_USER.RESET_KEY, JHI_USER.CREATED_BY,
            JHI_USER.CREATED_DATE, JHI_USER.RESET_DATE, JHI_USER.LAST_MODIFIED_BY, JHI_USER.LAST_MODIFIED_DATE,
            JHI_USER_AUTHORITY.AUTHORITY_NAME)
            .from(JHI_USER)
            .join(JHI_USER_AUTHORITY)
            .on(JHI_USER.ID.equal(JHI_USER_AUTHORITY.USER_ID))
            .where(JHI_USER.ID.equal(id))
            .fetch();*/

        /*Record queryResult1 = (Record) jooq.select(JHI_USER.ID, JHI_USER.LOGIN, JHI_USER.PASSWORD_HASH,
            JHI_USER.FIRST_NAME, JHI_USER.LAST_NAME, JHI_USER.EMAIL, JHI_USER.ACTIVATED,
            JHI_USER.LANG_KEY, JHI_USER.ACTIVATION_KEY, JHI_USER.RESET_KEY, JHI_USER.CREATED_BY,
            JHI_USER.CREATED_DATE, JHI_USER.RESET_DATE, JHI_USER.LAST_MODIFIED_BY, JHI_USER.LAST_MODIFIED_DATE,
            JHI_USER_AUTHORITY.AUTHORITY_NAME)
            .from(JHI_USER)
            .join(JHI_USER_AUTHORITY)
            .on(JHI_USER.ID.equal(JHI_USER_AUTHORITY.USER_ID))
            .where(JHI_USER.ID.equal(id))
            .fetch();*/

        log.info("************************** JhiUserRecord: " + queryResult);
        /*Map<String,List<JhiUserDto>> mapResult = queryResult.intoGroups(JHI_USER_AUTHORITY.AUTHORITY_NAME, JhiUserDto.class);
        log.info("");
        log.info("@@@@@@@@@@@@@@@@@@@@@@@@@@@@ mapREsult: "+mapResult);
        for (Map.Entry<String,List<JhiUserDto>> entry : mapResult.entrySet())
        {
            System.out.println();
            System.out.println(entry.getKey() + " - ");
            for(int i = 0; i< entry.getValue().size(); i++){
                System.out.println(entry.getValue().get(i));
            }

        }*/

        /*log.info("************************** JhiUserRecord1: "+ queryResult1);*/



        /*JhiUserDto userDTO = modelMapper.map(queryResult, JhiUserDto.class);


        log.info(">>>>>>>>>>>>>>>>>>> Mapper: "+userDTO);*/
        return queryResult;

    }
}

