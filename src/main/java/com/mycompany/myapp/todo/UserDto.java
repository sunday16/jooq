package com.mycompany.myapp.todo;

import com.mycompany.myapp.domain.Authority;
import com.mycompany.myapp.domain.PersistentToken;

import java.sql.Timestamp;
import java.util.Set;

/**
 * Created by sunday on 02.03.16.
 */
public class UserDto {

    private Long id;
    private String login;
    private String password;
    private String firstName;
    private String lastName;
    private String email;
    private boolean activated;
    private String langKey;
    private String activationKey;
    private  String resetKey;
    private Timestamp resetDate;
    private Set<Authority> authorities;
    private Set<PersistentToken> persistentTokens;


    public UserDto() {
    }

    public UserDto(Long id, String login, String password, String firstName, String lastName, String email,
                   boolean activated, String langKey, String activationKey, String resetKey, Timestamp resetDate,
                   Set<Authority> authorities, Set<PersistentToken> persistentTokens) {
        this.id = id;
        this.login = login;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.activated = activated;
        this.langKey = langKey;
        this.activationKey = activationKey;
        this.resetKey = resetKey;
        this.resetDate = resetDate;
        this.authorities = authorities;
        this.persistentTokens = persistentTokens;
    }

    public UserDto(Long id, String login, String password, String firstName, String lastName, String email, boolean activated, String langKey, String activationKey, String resetKey, Timestamp resetDate) {
        this.id = id;
        this.login = login;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.activated = activated;
        this.langKey = langKey;
        this.activationKey = activationKey;
        this.resetKey = resetKey;
        this.resetDate = resetDate;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setActivated(boolean activated) {
        this.activated = activated;
    }

    public void setLangKey(String langKey) {
        this.langKey = langKey;
    }

    public void setActivationKey(String activationKey) {
        this.activationKey = activationKey;
    }

    public void setResetKey(String resetKey) {
        this.resetKey = resetKey;
    }

    public void setResetDate(Timestamp resetDate) {
        this.resetDate = resetDate;
    }

    public void setAuthorities(Set<Authority> authorities) {
        this.authorities = authorities;
    }

    public void setPersistentTokens(Set<PersistentToken> persistentTokens) {
        this.persistentTokens = persistentTokens;
    }

    @Override
    public String toString() {
        return "UserDto{" +
            "id=" + id +
            ", login='" + login + '\'' +
            ", password='" + password + '\'' +
            ", firstName='" + firstName + '\'' +
            ", lastName='" + lastName + '\'' +
            ", email='" + email + '\'' +
            ", activated=" + activated +
            ", langKey='" + langKey + '\'' +
            ", activationKey='" + activationKey + '\'' +
            ", resetKey='" + resetKey + '\'' +
            ", resetDate=" + resetDate +
            ", authorities=" + authorities +
            ", persistentTokens=" + persistentTokens +
            '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserDto userDto = (UserDto) o;

        if (activated != userDto.activated) return false;
        if (!id.equals(userDto.id)) return false;
        if (!login.equals(userDto.login)) return false;
        if (!password.equals(userDto.password)) return false;
        if (!firstName.equals(userDto.firstName)) return false;
        if (!lastName.equals(userDto.lastName)) return false;
        if (!email.equals(userDto.email)) return false;
        if (!langKey.equals(userDto.langKey)) return false;
        if (!activationKey.equals(userDto.activationKey)) return false;
        if (!resetKey.equals(userDto.resetKey)) return false;
        if (resetDate != null ? !resetDate.equals(userDto.resetDate) : userDto.resetDate != null) return false;
        if (!authorities.equals(userDto.authorities)) return false;
        return persistentTokens.equals(userDto.persistentTokens);

    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + login.hashCode();
        result = 31 * result + password.hashCode();
        result = 31 * result + firstName.hashCode();
        result = 31 * result + lastName.hashCode();
        result = 31 * result + email.hashCode();
        result = 31 * result + (activated ? 1 : 0);
        result = 31 * result + langKey.hashCode();
        result = 31 * result + activationKey.hashCode();
        result = 31 * result + resetKey.hashCode();
        result = 31 * result + (resetDate != null ? resetDate.hashCode() : 0);
        result = 31 * result + authorities.hashCode();
        result = 31 * result + persistentTokens.hashCode();
        return result;
    }

    public Long getId() {
        return id;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public boolean isActivated() {
        return activated;
    }

    public String getLangKey() {
        return langKey;
    }

    public String getActivationKey() {
        return activationKey;
    }

    public String getResetKey() {
        return resetKey;
    }

    public Timestamp getResetDate() {
        return resetDate;
    }

    public Set<Authority> getAuthorities() {
        return authorities;
    }

    public Set<PersistentToken> getPersistentTokens() {
        return persistentTokens;
    }
}
