package com.mycompany.myapp.todo;

import net.mycompany.myapp.jooq.todo.tables.records.JhiUserRecord;
import org.modelmapper.PropertyMap;

/**
 * Created by sunday on 09.03.16.
 */
public class JhiUserMap extends PropertyMap<JhiUserRecord, JhiUserDto> {

    @Override
    protected void configure() {
        map().setId(source.getId());
        map().setLogin(source.getLogin());
        map().setPasswordHash(source.getPasswordHash());
        map().setFirstName(source.getFirstName());
        map().setLastName(source.getLastName());
        map().setEmail(source.getEmail());
        map().setActivated(source.getActivated());
        map().setLangKey(source.getLangKey());
        map().setActivationKey(source.getActivationKey());
        map().setResetKey(source.getResetKey());
        map().setCreatedBy(source.getCreatedBy());
        map().setCreatedDate(source.getCreatedDate());
        map().setResetDate(source.getResetDate());
        map().setLastModifiedBy(source.getLastModifiedBy());
        map().setLastModifiedDate(source.getLastModifiedDate());


    }
}
