package com.mycompany.myapp.todo;

/**
 * Created by sunday on 02.03.16.
 */
public class JooqUserNotFound extends RuntimeException {

    public JooqUserNotFound(String message) {
        super(message);
    }
}
