package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.repository.UserJooqRepository;
import com.mycompany.myapp.todo.UserDto;
import net.mycompany.myapp.jooq.todo.tables.pojos.JhiUser;
import net.mycompany.myapp.jooq.todo.tables.pojos.JhiUserAuthority;
import org.jooq.Record2;
import org.jooq.Result;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;


/**
 * Created by sunday on 03.03.16.
 */
@RestController
@RequestMapping("/test")
public class UserJooqResource {

    private final Logger log = LoggerFactory.getLogger(UserJooqResource.class);

    @Autowired
    private UserJooqRepository userJooqRepository;


    @RequestMapping(value = "/jooq/user/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Transactional(readOnly = true)
    public ResponseEntity<Result<Record2<Long,String>>> getUSer(@PathVariable(value = "id") long id){

        log.info(">>>>>>>>>> getUser id: "+id);
        Result<Record2<Long,String>> result = userJooqRepository.findById(id);

        if(result == null) {
            log.info(">>>>>>>>>>>>>>>>>>>>>>>> result = " + result);
        }
        return new ResponseEntity<Result<Record2<Long, String>>>(result, HttpStatus.OK);
    }

    @RequestMapping(value = "/jooq/user",
    method = RequestMethod.GET,
    produces = MediaType.APPLICATION_JSON_VALUE)
    @Transactional(readOnly = true)
    public ResponseEntity<List<UserDto>> getAllUser(){
        log.info(">>>>>>>>>>>>>>>>>>>>>>>>> Get All Users");
        List<UserDto> results = userJooqRepository.findAll();

        return new ResponseEntity<List<UserDto>>(results, HttpStatus.OK);
    }

    @RequestMapping(value = "/jooqmapper/user/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Transactional(readOnly = true)
    public ResponseEntity<Map<JhiUser, List<JhiUserAuthority>>> getUSerMap(@PathVariable(value = "id") long id){

        log.info(">>>>>>>>>> getUser id: "+id);
        Map<JhiUser, List<JhiUserAuthority>> result = userJooqRepository.findByIdUser(id);

        if(result == null) {
            log.info(">>>>>>>>>>>>>>>>>>>>>>>> result = " + result);
        }
        return new ResponseEntity<Map<JhiUser, List<JhiUserAuthority>>>(result, HttpStatus.OK);
    }


}
