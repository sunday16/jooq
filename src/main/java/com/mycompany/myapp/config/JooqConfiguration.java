package com.mycompany.myapp.config;

import com.mycompany.myapp.todo.JOOQToSpringExceptionTransformer;
import org.jooq.SQLDialect;
import org.jooq.impl.DataSourceConnectionProvider;
import org.jooq.impl.DefaultConfiguration;
import org.jooq.impl.DefaultDSLContext;
import org.jooq.impl.DefaultExecuteListenerProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.LazyConnectionDataSourceProxy;
import org.springframework.jdbc.datasource.TransactionAwareDataSourceProxy;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;

/**
 * Created by sunday on 29.02.16.
 */

@Configuration
@EnableTransactionManagement
public class JooqConfiguration {

    public static final Logger log = LoggerFactory.getLogger(JooqConfiguration.class);

    @Bean
    public DefaultConfiguration configuration(DataSource dataSource) {
        DefaultConfiguration jooqConfiguration = new DefaultConfiguration();

        jooqConfiguration.set(connectionProvider(dataSource));
        jooqConfiguration.set(new DefaultExecuteListenerProvider(jooqToSpringExceptionTransformer()));

        SQLDialect dialect = SQLDialect.valueOf(String.valueOf(SQLDialect.POSTGRES));
        jooqConfiguration.set(dialect);

        return jooqConfiguration;
    }

    public LazyConnectionDataSourceProxy lazyConnectionDataSource(DataSource dataSource) {
        log.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>. lazyConnectionDataSource: datasource = "+dataSource);
        return new LazyConnectionDataSourceProxy(dataSource);
    }


    public TransactionAwareDataSourceProxy transactionAwareDataSource(DataSource dataSource) {
        return new TransactionAwareDataSourceProxy(lazyConnectionDataSource(dataSource));
    }

    @Bean
    public DataSourceTransactionManager transactionManager(DataSource dataSource) {
        return new DataSourceTransactionManager(lazyConnectionDataSource(dataSource));
    }

    @Bean
    public DataSourceConnectionProvider connectionProvider(DataSource dataSource) {
        return new DataSourceConnectionProvider(transactionAwareDataSource(dataSource));
    }

    @Bean
    public JOOQToSpringExceptionTransformer jooqToSpringExceptionTransformer() {
        return new JOOQToSpringExceptionTransformer();
    }

    @Bean
    public DefaultDSLContext dsl(DataSource dataSource) {
        return new DefaultDSLContext(configuration(dataSource));
    }



}
