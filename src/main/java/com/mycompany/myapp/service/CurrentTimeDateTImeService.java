package com.mycompany.myapp.service;

import org.joda.time.LocalDateTime;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;

/**
 * Created by sunday on 27.02.16.
 */

@Component
public class CurrentTimeDateTImeService implements DateTimeService {
    @Override
    public LocalDateTime getCurrentDateTime() {
        return LocalDateTime.now();
    }

    @Override
    public Timestamp getCurrentTimestamp() {
        return new Timestamp(System.currentTimeMillis());
    }
}
