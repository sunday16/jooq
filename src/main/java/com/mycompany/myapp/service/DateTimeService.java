package com.mycompany.myapp.service;

import org.joda.time.LocalDateTime;

import java.sql.Timestamp;

/**
 * Created by sunday on 27.02.16.
 */
public interface DateTimeService {

    public LocalDateTime getCurrentDateTime();

    public Timestamp getCurrentTimestamp();
}
