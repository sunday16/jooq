package com.mycompany.myapp.repository;

import com.mycompany.myapp.todo.JhiUserDto;
import com.mycompany.myapp.todo.UserDto;
import net.mycompany.myapp.jooq.todo.tables.pojos.JhiUser;
import net.mycompany.myapp.jooq.todo.tables.pojos.JhiUserAuthority;
import org.jooq.Record2;
import org.jooq.Result;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;


/**
 * Created by sunday on 29.02.16.
 */

public interface UserJooqRepository {

    @Transactional
    Result<Record2<Long,String>> findById(Long id);

    List<UserDto> findAll();

    Map<JhiUser, List<JhiUserAuthority>> findByIdUser(Long id);
}
