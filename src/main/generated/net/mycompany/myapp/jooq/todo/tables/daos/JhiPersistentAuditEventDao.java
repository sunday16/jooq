/**
 * This class is generated by jOOQ
 */
package net.mycompany.myapp.jooq.todo.tables.daos;


import java.sql.Timestamp;
import java.util.List;

import javax.annotation.Generated;

import net.mycompany.myapp.jooq.todo.tables.JhiPersistentAuditEvent;
import net.mycompany.myapp.jooq.todo.tables.records.JhiPersistentAuditEventRecord;

import org.jooq.Configuration;
import org.jooq.impl.DAOImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
	value = {
		"http://www.jooq.org",
		"jOOQ version:3.7.1"
	},
	comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class JhiPersistentAuditEventDao extends DAOImpl<JhiPersistentAuditEventRecord, net.mycompany.myapp.jooq.todo.tables.pojos.JhiPersistentAuditEvent, Long> {

	/**
	 * Create a new JhiPersistentAuditEventDao without any configuration
	 */
	public JhiPersistentAuditEventDao() {
		super(JhiPersistentAuditEvent.JHI_PERSISTENT_AUDIT_EVENT, net.mycompany.myapp.jooq.todo.tables.pojos.JhiPersistentAuditEvent.class);
	}

	/**
	 * Create a new JhiPersistentAuditEventDao with an attached configuration
	 */
	public JhiPersistentAuditEventDao(Configuration configuration) {
		super(JhiPersistentAuditEvent.JHI_PERSISTENT_AUDIT_EVENT, net.mycompany.myapp.jooq.todo.tables.pojos.JhiPersistentAuditEvent.class, configuration);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected Long getId(net.mycompany.myapp.jooq.todo.tables.pojos.JhiPersistentAuditEvent object) {
		return object.getEventId();
	}

	/**
	 * Fetch records that have <code>event_id IN (values)</code>
	 */
	public List<net.mycompany.myapp.jooq.todo.tables.pojos.JhiPersistentAuditEvent> fetchByEventId(Long... values) {
		return fetch(JhiPersistentAuditEvent.JHI_PERSISTENT_AUDIT_EVENT.EVENT_ID, values);
	}

	/**
	 * Fetch a unique record that has <code>event_id = value</code>
	 */
	public net.mycompany.myapp.jooq.todo.tables.pojos.JhiPersistentAuditEvent fetchOneByEventId(Long value) {
		return fetchOne(JhiPersistentAuditEvent.JHI_PERSISTENT_AUDIT_EVENT.EVENT_ID, value);
	}

	/**
	 * Fetch records that have <code>principal IN (values)</code>
	 */
	public List<net.mycompany.myapp.jooq.todo.tables.pojos.JhiPersistentAuditEvent> fetchByPrincipal(String... values) {
		return fetch(JhiPersistentAuditEvent.JHI_PERSISTENT_AUDIT_EVENT.PRINCIPAL, values);
	}

	/**
	 * Fetch records that have <code>event_date IN (values)</code>
	 */
	public List<net.mycompany.myapp.jooq.todo.tables.pojos.JhiPersistentAuditEvent> fetchByEventDate(Timestamp... values) {
		return fetch(JhiPersistentAuditEvent.JHI_PERSISTENT_AUDIT_EVENT.EVENT_DATE, values);
	}

	/**
	 * Fetch records that have <code>event_type IN (values)</code>
	 */
	public List<net.mycompany.myapp.jooq.todo.tables.pojos.JhiPersistentAuditEvent> fetchByEventType(String... values) {
		return fetch(JhiPersistentAuditEvent.JHI_PERSISTENT_AUDIT_EVENT.EVENT_TYPE, values);
	}
}
