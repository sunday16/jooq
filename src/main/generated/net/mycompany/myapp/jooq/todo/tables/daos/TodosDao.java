/**
 * This class is generated by jOOQ
 */
package net.mycompany.myapp.jooq.todo.tables.daos;


import java.sql.Timestamp;
import java.util.List;

import javax.annotation.Generated;

import net.mycompany.myapp.jooq.todo.tables.Todos;
import net.mycompany.myapp.jooq.todo.tables.records.TodosRecord;

import org.jooq.Configuration;
import org.jooq.impl.DAOImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
	value = {
		"http://www.jooq.org",
		"jOOQ version:3.7.1"
	},
	comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class TodosDao extends DAOImpl<TodosRecord, net.mycompany.myapp.jooq.todo.tables.pojos.Todos, Long> {

	/**
	 * Create a new TodosDao without any configuration
	 */
	public TodosDao() {
		super(Todos.TODOS, net.mycompany.myapp.jooq.todo.tables.pojos.Todos.class);
	}

	/**
	 * Create a new TodosDao with an attached configuration
	 */
	public TodosDao(Configuration configuration) {
		super(Todos.TODOS, net.mycompany.myapp.jooq.todo.tables.pojos.Todos.class, configuration);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected Long getId(net.mycompany.myapp.jooq.todo.tables.pojos.Todos object) {
		return object.getId();
	}

	/**
	 * Fetch records that have <code>id IN (values)</code>
	 */
	public List<net.mycompany.myapp.jooq.todo.tables.pojos.Todos> fetchById(Long... values) {
		return fetch(Todos.TODOS.ID, values);
	}

	/**
	 * Fetch a unique record that has <code>id = value</code>
	 */
	public net.mycompany.myapp.jooq.todo.tables.pojos.Todos fetchOneById(Long value) {
		return fetchOne(Todos.TODOS.ID, value);
	}

	/**
	 * Fetch records that have <code>creation_time IN (values)</code>
	 */
	public List<net.mycompany.myapp.jooq.todo.tables.pojos.Todos> fetchByCreationTime(Timestamp... values) {
		return fetch(Todos.TODOS.CREATION_TIME, values);
	}

	/**
	 * Fetch records that have <code>description IN (values)</code>
	 */
	public List<net.mycompany.myapp.jooq.todo.tables.pojos.Todos> fetchByDescription(String... values) {
		return fetch(Todos.TODOS.DESCRIPTION, values);
	}

	/**
	 * Fetch records that have <code>modification_time IN (values)</code>
	 */
	public List<net.mycompany.myapp.jooq.todo.tables.pojos.Todos> fetchByModificationTime(Timestamp... values) {
		return fetch(Todos.TODOS.MODIFICATION_TIME, values);
	}

	/**
	 * Fetch records that have <code>title IN (values)</code>
	 */
	public List<net.mycompany.myapp.jooq.todo.tables.pojos.Todos> fetchByTitle(String... values) {
		return fetch(Todos.TODOS.TITLE, values);
	}
}
