/**
 * This class is generated by jOOQ
 */
package net.mycompany.myapp.jooq.todo.tables.records;


import javax.annotation.Generated;

import net.mycompany.myapp.jooq.todo.tables.JhiAuthority;

import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Row1;
import org.jooq.impl.UpdatableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
	value = {
		"http://www.jooq.org",
		"jOOQ version:3.7.1"
	},
	comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class JhiAuthorityRecord extends UpdatableRecordImpl<JhiAuthorityRecord> implements Record1<String> {

	private static final long serialVersionUID = 352259303;

	/**
	 * Setter for <code>public.jhi_authority.name</code>.
	 */
	public void setName(String value) {
		setValue(0, value);
	}

	/**
	 * Getter for <code>public.jhi_authority.name</code>.
	 */
	public String getName() {
		return (String) getValue(0);
	}

	// -------------------------------------------------------------------------
	// Primary key information
	// -------------------------------------------------------------------------

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Record1<String> key() {
		return (Record1) super.key();
	}

	// -------------------------------------------------------------------------
	// Record1 type implementation
	// -------------------------------------------------------------------------

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Row1<String> fieldsRow() {
		return (Row1) super.fieldsRow();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Row1<String> valuesRow() {
		return (Row1) super.valuesRow();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Field<String> field1() {
		return JhiAuthority.JHI_AUTHORITY.NAME;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String value1() {
		return getName();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public JhiAuthorityRecord value1(String value) {
		setName(value);
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public JhiAuthorityRecord values(String value1) {
		value1(value1);
		return this;
	}

	// -------------------------------------------------------------------------
	// Constructors
	// -------------------------------------------------------------------------

	/**
	 * Create a detached JhiAuthorityRecord
	 */
	public JhiAuthorityRecord() {
		super(JhiAuthority.JHI_AUTHORITY);
	}

	/**
	 * Create a detached, initialised JhiAuthorityRecord
	 */
	public JhiAuthorityRecord(String name) {
		super(JhiAuthority.JHI_AUTHORITY);

		setValue(0, name);
	}
}
