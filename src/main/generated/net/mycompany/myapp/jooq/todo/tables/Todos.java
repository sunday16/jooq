/**
 * This class is generated by jOOQ
 */
package net.mycompany.myapp.jooq.todo.tables;


import java.sql.Timestamp;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Generated;

import net.mycompany.myapp.jooq.todo.Keys;
import net.mycompany.myapp.jooq.todo.Public;
import net.mycompany.myapp.jooq.todo.tables.records.TodosRecord;

import org.jooq.Field;
import org.jooq.Identity;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.UniqueKey;
import org.jooq.impl.TableImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
	value = {
		"http://www.jooq.org",
		"jOOQ version:3.7.1"
	},
	comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Todos extends TableImpl<TodosRecord> {

	private static final long serialVersionUID = 1589651811;

	/**
	 * The reference instance of <code>public.todos</code>
	 */
	public static final Todos TODOS = new Todos();

	/**
	 * The class holding records for this type
	 */
	@Override
	public Class<TodosRecord> getRecordType() {
		return TodosRecord.class;
	}

	/**
	 * The column <code>public.todos.id</code>.
	 */
	public final TableField<TodosRecord, Long> ID = createField("id", org.jooq.impl.SQLDataType.BIGINT.nullable(false).defaulted(true), this, "");

	/**
	 * The column <code>public.todos.creation_time</code>.
	 */
	public final TableField<TodosRecord, Timestamp> CREATION_TIME = createField("creation_time", org.jooq.impl.SQLDataType.TIMESTAMP.nullable(false).defaulted(true), this, "");

	/**
	 * The column <code>public.todos.description</code>.
	 */
	public final TableField<TodosRecord, String> DESCRIPTION = createField("description", org.jooq.impl.SQLDataType.VARCHAR.length(500), this, "");

	/**
	 * The column <code>public.todos.modification_time</code>.
	 */
	public final TableField<TodosRecord, Timestamp> MODIFICATION_TIME = createField("modification_time", org.jooq.impl.SQLDataType.TIMESTAMP.nullable(false).defaulted(true), this, "");

	/**
	 * The column <code>public.todos.title</code>.
	 */
	public final TableField<TodosRecord, String> TITLE = createField("title", org.jooq.impl.SQLDataType.VARCHAR.length(100), this, "");

	/**
	 * Create a <code>public.todos</code> table reference
	 */
	public Todos() {
		this("todos", null);
	}

	/**
	 * Create an aliased <code>public.todos</code> table reference
	 */
	public Todos(String alias) {
		this(alias, TODOS);
	}

	private Todos(String alias, Table<TodosRecord> aliased) {
		this(alias, aliased, null);
	}

	private Todos(String alias, Table<TodosRecord> aliased, Field<?>[] parameters) {
		super(alias, Public.PUBLIC, aliased, parameters, "");
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Identity<TodosRecord, Long> getIdentity() {
		return Keys.IDENTITY_TODOS;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public UniqueKey<TodosRecord> getPrimaryKey() {
		return Keys.PK_TODOS;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<UniqueKey<TodosRecord>> getKeys() {
		return Arrays.<UniqueKey<TodosRecord>>asList(Keys.PK_TODOS);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Todos as(String alias) {
		return new Todos(alias, this);
	}

	/**
	 * Rename this table
	 */
	public Todos rename(String name) {
		return new Todos(name, null);
	}
}
